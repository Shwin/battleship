package players;

import board.Board;
import board.IBoard;
import board.Point;
import board.square.ships.Ship;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Human player
 */
public class Human extends Player {

    private transient Scanner sc = new Scanner(System.in);

    public Human(String human) {
        super(human);
    }

    /**
     * convert from 'a1' to Point(0,0)
     *
     * @param input
     * @return
     */
    public static Point fromStringtoPoint(String input) {
        String[] coords = input.split("");
        String lables = "abcdefghij";

        int y = lables.indexOf(coords[0]);
        int x = Integer.parseInt(coords[1]) - 1;
        if (y == -1 || x < 0 || x >= 10) {
            throw new NumberFormatException();
        }
        return new Point(x, y);
    }

    @Override
    public IBoard begin() {
        System.out.println("Could you autogen your posission?[y/n]");
        boolean b = true;
        while (b) {
            String inp = sc.next();
            switch (inp) {
                case "y": {
                    b = false;
                    ArrayList<Ship> ships = Bot.genFleetPosition();
                    this.setMyBoard(new Board(ships));
                    return getMyBoard();
                }
                case "n": {
                    b = false;
                    ArrayList<Ship> ships = new ArrayList<>();
                    int maxSize = 4;
                    int id = 0;
                    for (int i = 1; i <= maxSize; i++) {
                        for (int j = i; j <= maxSize; j++) {
                            if (i == 1) {
                                System.out.print("Single-desk: ");
                                inp = sc.next();
                                Point p = fromStringtoPoint(inp);
                                ships.add(new Ship(id++, p.getX(), p.getY(), p.getX(), p.getY()));
                                continue;
                            }
                            System.out.println(String.format("Input first and last points of %d-desk ship", i));
                            Point p1 = fromStringtoPoint(sc.next());
                            Point p2 = fromStringtoPoint(sc.next());
                            if ((p1.getX() == p2.getX() || p1.getY() == p2.getY())) {
                                ships.add(new Ship(id++, p1.getX(), p1.getY(), p2.getX(), p2.getY()));
                            }
                        }
                    }
                    this.setMyBoard(new Board(ships));
                    return getMyBoard();
                }
                default: {
                    System.out.println("Please [y/n]");
                    b = true;
                }
            }
        }
        return null;
    }

    @Override
    public Point choseArea() throws NumberFormatException {
        String input = sc.next();
        return fromStringtoPoint(input);
    }
}


