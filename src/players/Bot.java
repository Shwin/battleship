package players;

import board.Board;
import board.IBoard;
import board.Point;
import board.square.ships.Ship;
import game.Turn;
import game.TurnResult;

import java.util.*;

/**
 * Bot
 */
public class Bot extends Player {

    private static int[] offsetX = {0, 1, 0, -1};
    private static int[] offsetY = {1, 0, -1, 0};
    private static int[] fullOffsetY = {-1, 0, 1, 1, 1, 0, -1, -1};
    private static int[] fullOffsetX = {1, 1, 1, 0, -1, -1, -1, 0};
    private int x = 0;
    private int y = 0;
    private int next = 4;
    private boolean findDoubleDeckedShips = false;
    private boolean killing = false;
    private LinkedList<Point> recomends = new LinkedList<>();

    public Bot(String name) {
        super(name);
    }

    /**
     * generate fleet's position
     * can be used for other players
     *
     * @return
     */
    public static ArrayList<Ship> genFleetPosition() {
        ArrayList<Ship> ships = new ArrayList<>();
        ships.add(new Ship(0, 6, 0, 9, 0));
        ships.add(new Ship(1, 7, 2, 7, 4));
        ships.add(new Ship(2, 7, 7, 7, 9));
        ships.add(new Ship(3, 9, 2, 9, 3));
        ships.add(new Ship(4, 9, 5, 9, 6));
        ships.add(new Ship(5, 9, 8, 9, 9));
        ships.add(new Ship(6, 4, 0, 4, 0));
        ships.add(new Ship(7, 2, 0, 2, 0));

        Random random = new Random();
        int dispX = 8;
        int offsetX = 2;
        int dispY = 6;
        int y1 = random.nextInt(dispX) + offsetX;
        int x1 = random.nextInt(dispY);
        ships.add(new Ship(8, x1, y1, x1, y1));
        int x2, y2;
        do {
            y2 = random.nextInt(dispX) + offsetX;
            x2 = random.nextInt(dispY);
        } while (Math.abs(x1 - x2) <= 1 && Math.abs(y1 - y2) <= 1);
        ships.add(new Ship(9, x2, y2, x2, y2));
        return ships;
    }

    @Override
    public IBoard begin() {

        ArrayList<Ship> ships = genFleetPosition();

        Board board = new Board(ships);
        this.setMyBoard(board);

        return board;
    }

    /**
     * chose point for fire
     *
     * @return
     */
    @Override
    public Point choseArea() {
        Turn privTurn = getPreviousTurns().peekFirst();
        if (privTurn != null) {
            if (privTurn.result == TurnResult.KILL) {// kill a ship at previous turn
                removeEmptyAreas(privTurn.point.getX(), privTurn.point.getY(), new HashSet<>());
                recomends.clear();
                killing = false;
            }
            killing = privTurn.result == TurnResult.INJURE || !recomends.isEmpty();
        }
        if (killing) {//we are killing something now?
            return killing(x, y);
        } else {
            //only one area ships
            if (getEnemyAliveShips()[1] == 0
                    && getEnemyAliveShips()[2] == 0 &&
                    getEnemyAliveShips()[3] == 0) {
                Iterator<Point> iter = getUnbombedAreas().iterator();
                if (iter.hasNext()) {
                    return iter.next();
                }
            }
            //chose a point
            while (x < Board.length * Board.length) {
                if (y >= Board.length) {
                    x++;
                    if (x >= Board.length) {
                        break;
                    }
                    y = x % next;
                    if (findDoubleDeckedShips && y == 0) { // magic for bombing in direct order
                        y = 2;
                    }
                    return new Point(x, y);

                }
                if (getUnbombedAreas().contains(new Point(x, y))) {
                    return new Point(x, y);
                }
                y += next;
            }

            next = next / 2;
            x = 0;
            y = next;
            findDoubleDeckedShips = true;

            return new Point(x, y);

        }
    }

    /**
     * remove empty areas form unbombedAreas
     */
    public void removeEmptyAreas(int x, int y, HashSet<Point> visited) {
        if (x < 0 || x >= Board.length || y < 0 || y >= Board.length) {
            return;
        }
        Point p = new Point(x, y);
        getUnbombedAreas().remove(p);
        if (this.getEnemyBoard().isDeadShip(x, y)
                && !visited.contains(p)) {
            visited.add(p);
            for (int i = 0; i < fullOffsetX.length; i++) {
                removeEmptyAreas(x + fullOffsetX[i], y + fullOffsetY[i], visited);
            }
        }
    }

    /**
     * trying to kill the injured ship
     *
     * @param x
     * @param y
     * @return
     */
    private Point killing(int x, int y) {
        Turn priv = getPreviousTurns().peekFirst();
        if (priv.result == TurnResult.PASS) {//exclude one line points
            int offsetX = priv.point.getX() - x;
            int offsetY = priv.point.getY() - y;
            getUnbombedAreas().remove(new Point(x + offsetX, y + offsetY));
        }
        if (priv.result == TurnResult.INJURE) {//trying to kill
            if (x == priv.point.getX() && y == priv.point.getY()) {//prev and now are the same points
                return nextPoint(x, y);
            }
            if (x == priv.point.getX()) {
                int newYShift = priv.point.getY() - y;
                newYShift /= Math.abs(newYShift);
                int newY = priv.point.getY() + newYShift;
                if (newY < 0 || newY >= Board.length) {
                    newY = y - newYShift;
                }
                return new Point(x, newY);
            } else {
                int newXShift = priv.point.getX() - x;
                newXShift /= Math.abs(newXShift);
                int newX = priv.point.getX() + newXShift;
                if (newX < 0 || newX >= Board.length) {
                    newX = x - newXShift;
                }
                return new Point(newX, y);
            }
        } else {
            return recomends.pollFirst();
        }
    }

    private Point nextPoint(int x, int y) {
        if (recomends.isEmpty()) {
            for (int i = 0; i < offsetX.length; i++) {
                int newX = x + offsetX[i];
                int newY = y + offsetY[i];
                if (super.getUnbombedAreas().contains(new Point(x, y)) ||
                        newX < 0 || newX >= Board.length || newY < 0 || newY >= Board.length) {
                    continue;
                }
                recomends.add(new Point(newX, newY));
            }
        }
        return recomends.poll();
    }
}
