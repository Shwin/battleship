package players;

import board.Board;
import board.EnemyBoard;
import board.IBoard;
import board.Point;
import game.Turn;
import game.TurnResult;

import java.io.Serializable;
import java.util.HashSet;
import java.util.IllegalFormatException;
import java.util.LinkedList;

/**
 * Abstract class with general player logic
 */
public abstract class Player implements Serializable {

    private final int TURN_MEMORY = 10;
    private final String name;
    private transient int[] enemyAliveShips;
    private IBoard myBoard;
    private IBoard enemyBoard;

    private LinkedList<Turn> previousTurns = new LinkedList<>();
    private HashSet<Point> unbombedAreas = new HashSet<>();

    public Player(String name) {
        this.name = name;
        for (int i = 0; i < Board.length; i++) {
            for (int j = 0; j < Board.length; j++) {
                unbombedAreas.add(new Point(i, j));
            }
        }
    }

    /**
     * @return
     */
    public TurnResult turn() {
        this.enemyAliveShips = enemyBoard.getAliveFleetStatistics();
        Point bombedArea = this.choseArea();
        TurnResult result = enemyBoard.fire(bombedArea.getX(), bombedArea.getY());
        unbombedAreas.remove(bombedArea);
        previousTurns.addFirst(new Turn(bombedArea, result));
        if (previousTurns.size() == TURN_MEMORY) {//controle the memory_of_turns size
            previousTurns.pollLast();
        }
        return result;
    }

    /**
     * generate fleet's position
     *
     * @return
     */
    public abstract IBoard begin();

    /**
     * chose an area for fire
     */
    public abstract Point choseArea() throws IllegalFormatException;


    public boolean isAlive() {
        return myBoard.getAliveFleetStatistics() != new int[]{0, 0, 0, 0};
    }

    public int[] getEnemyAliveShips() {
        return enemyAliveShips;
    }


    public IBoard getMyBoard() {
        return myBoard;
    }

    public void setMyBoard(IBoard myBoard) {
        this.myBoard = myBoard;
    }

    public IBoard getEnemyBoard() {
        return enemyBoard;
    }

    public void setEnemyBoard(EnemyBoard enemyBoard) {
        this.enemyBoard = enemyBoard;
    }

    public LinkedList<Turn> getPreviousTurns() {
        return previousTurns;
    }

    public HashSet<Point> getUnbombedAreas() {
        return unbombedAreas;
    }


    public String getName() {
        return name;
    }
}
