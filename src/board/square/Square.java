package board.square;

import board.square.ships.Ship;
import game.TurnResult;

import java.io.Serializable;

/**
 * Implementations of the interface store date about
 * a game's board aries
 */
public interface Square extends Serializable {

    /**
     * results of shooting
     */


    /**
     * get ship object if there is a ship at this area
     *
     * @return
     */
    Ship getShip();

    /**
     * return visual image of this area
     *
     * @param x,y for getting different parts of a ship
     * @return
     */
    String getVisualImage(int x, int y);


    /**
     * Behaviour of the area after bombing it. Usually change visual image of the area
     *
     * @param x
     * @param y
     * @return
     */
    TurnResult fire(int x, int y);

}
