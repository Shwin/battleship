package board.square;

import board.square.ships.EmptyObjectShip;
import board.square.ships.Ship;
import game.TurnResult;

/**
 * Area without ships
 */
public class FreeSpace implements Square {

    private String visualImage = "-";

    @Override
    public Ship getShip() {
        return new EmptyObjectShip();
    }

    @Override
    public String getVisualImage(int x, int y) {
        return visualImage;
    }


    @Override
    public TurnResult fire(int x, int y) {
        visualImage = "'";
        return TurnResult.PASS;
    }
}
