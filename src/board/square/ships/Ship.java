package board.square.ships;

import board.Point;
import game.TurnResult;

import java.util.HashSet;

/**
 * Ship
 */
public class Ship implements board.square.Square {


    private HashSet<Point> aliveSquares = new HashSet<>();
    private Integer size, id;

    /**
     * @param id
     * @param x1
     * @param x2
     * @param y2
     * @param y1
     */
    public Ship(int id, int x1, int y1, int x2, int y2) {
        this.id = id;
        if (x1 == x2) {
            for (int i = y1; i <= y2; i++) {
                aliveSquares.add(new Point(x1, i));
            }
        } else if (y1 == y2) {
            for (int i = x1; i <= x2; i++) {
                aliveSquares.add(new Point(i, y1));
            }
        } else {
            throw new IllegalArgumentException();
        }
        size = aliveSquares.size();
    }

    public HashSet<Point> getAliveSquares() {
        return aliveSquares;
    }

    public Integer getSize() {
        return size;
    }

    public Integer getId() {
        return id;
    }

    /**
     * @param x
     * @param y
     * @return KILL or INJURE
     */
    public TurnResult fire(int x, int y) {
        Point p = new Point(x, y);
        aliveSquares.remove(p);
        if (aliveSquares.size() == 0) {
            return TurnResult.KILL;
        } else {
            return TurnResult.INJURE;
        }
    }

    @Override
    public Ship getShip() {
        return this;
    }

    @Override
    public String getVisualImage(int x, int y) {
        Point p = new Point(x, y);
        if (aliveSquares.contains(p)) {
            return "@";
        } else {
            return "*";
        }
    }
}
