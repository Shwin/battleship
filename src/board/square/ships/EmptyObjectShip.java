package board.square.ships;

/**
 * Pattern EmptyObject
 */
public class EmptyObjectShip extends Ship {

    public EmptyObjectShip() {
        super(-1, 0, 0, 0, 0);
    }
}
