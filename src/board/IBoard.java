package board;

import game.TurnResult;

import java.io.Serializable;

/**
 * Board
 */
public interface IBoard extends Serializable {
    TurnResult fire(int x, int y);

    String getVisualImage(int x, int y);

    int[] getAliveFleetStatistics();

    boolean isDeadShip(int x, int y);
}
