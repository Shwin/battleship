package board;

import java.io.Serializable;

/**
 * C
 */
public class Point implements Serializable {
    private final int y;
    private final int x;

    /**
     * @param x
     * @param y
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        return x == point.x && y == point.y;

    }

    /**
     * for normal set's behavior
     *
     * @return
     */
    @Override
    public int hashCode() {
        int result = y;
        result = 31 * result + x;
        return result;
    }

    @Override
    public String toString() {
        return String.format("(%d , %d)", x, y);
    }
}
