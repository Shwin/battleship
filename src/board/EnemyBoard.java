package board;

import game.TurnResult;

import java.util.HashSet;

/**
 * Facade for Board. Hide visual images.
 */
public class EnemyBoard implements IBoard {

    private final String unbombedArea = " ";

    private IBoard board;
    private HashSet<Point> bombedAreas = new HashSet<>();

    public EnemyBoard(IBoard board) {
        this.board = board;
    }

    /**
     * @param x
     * @param y
     * @return
     */
    @Override
    public TurnResult fire(int x, int y) {
        Point bombedArea = new Point(x, y);
        bombedAreas.add(bombedArea);
        return board.fire(x, y);
    }

    /**
     * @param x
     * @param y
     * @return
     */
    @Override
    public String getVisualImage(int x, int y) {
        Point bombedArea = new Point(x, y);
        if (bombedAreas.contains(bombedArea)) {
            return board.getVisualImage(x, y);
        } else {
            return unbombedArea;
        }
    }

    @Override
    public int[] getAliveFleetStatistics() {
        return board.getAliveFleetStatistics();
    }

    @Override
    public boolean isDeadShip(int x, int y) {
        return board.isDeadShip(x, y) && bombedAreas.contains(new Point(x, y));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("    a b c d e f g h i j \n");
        sb.append("   _____________________\n");
        for (int i = 0; i < Board.length; i++) {
            sb.append(String.format("%2d", i + 1));
            for (int j = 0; j < Board.length; j++) {
                if (j == 0) {
                    sb.append(" |");
                }
                sb.append(this.getVisualImage(i, j));
                sb.append(" ");
            }
            sb.append("|\n");
        }
        sb.append("   ______________________");
        return sb.toString();
    }


    public HashSet<Point> getBombedAreas() {
        return bombedAreas;
    }
}
