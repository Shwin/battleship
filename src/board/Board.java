package board;

import board.square.FreeSpace;
import board.square.Square;
import board.square.ships.Ship;
import game.TurnResult;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Board
 */
public class Board implements IBoard {

    public static final int length = 10;

    private Square[][] board = new Square[length][length];
    private HashMap<Integer, Ship> aliveShips = new HashMap<>();

    public Board(ArrayList<Ship> ships) {
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                board[i][j] = new FreeSpace();
            }
        }
        for (Ship ship : ships) {
            for (Point p : ship.getAliveSquares()) {
                int x = p.getX();
                int y = p.getY();
                board[x][y] = ship;
            }
        }
        for (int i = 0; i < ships.size(); i++) {
            aliveShips.put(i, ships.get(i));
        }
    }

    /**
     * @param x
     * @param y
     * @return FREE_SPACE, INJURED, KILL
     */
    public TurnResult fire(int x, int y) {
        TurnResult result = board[x][y].fire(x, y);
        if (result == TurnResult.KILL) {
            try {
                Ship ship = board[x][y].getShip();
                aliveShips.remove(ship.getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * for bot
     * return information about alive ships of enemy
     * [4,3,2,1] -> 4 one area ships, 3 two area ships, 2 three area ships, and 1 four area ship
     *
     * @return
     */
    public int[] getAliveFleetStatistics() {
        int numShipTypes = 4;
        final int[] stats = new int[numShipTypes];
        aliveShips.values().forEach(n -> {
            int i = n.getSize() - 1;
            stats[i] = stats[i] + 1;
        });
        return stats;
    }

    public String getVisualImage(int x, int y) {
        return board[x][y].getVisualImage(x, y);
    }

    public boolean isDeadShip(int x, int y) {
        if (x < 0 || x >= Board.length || y < 0 || y >= Board.length) {
            return false;
        }

        if (board[x][y] instanceof FreeSpace) {
            return false;
        }

        int id = board[x][y].getShip().getId();

        return !aliveShips.keySet().contains(id);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("    a b c d e f g h i j \n");
        sb.append("   _____________________\n");
        for (int i = 0; i < length; i++) {
            sb.append(String.format("%2d", i + 1));
            for (int j = 0; j < length; j++) {
                if (j == 0) {
                    sb.append(" |");
                }
                sb.append(board[i][j].getVisualImage(i, j));
                sb.append(" ");
            }
            sb.append("|\n");
        }
        sb.append("   ______________________");
        return sb.toString();
    }


}
