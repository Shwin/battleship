import game.Game;
import players.Bot;
import players.Human;

import java.io.File;
import java.util.Scanner;

/**
 * Ui
 */
public class UI {

    public static void main(String[] args) throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        System.out.println("New game Bot vs Bot OR Human vs Bot [b,h] or load [l]: ");
        String inp = sc.next();


        File f = new File("GAMESAVE");
        if (!f.exists() || !inp.equals("l")) {
            switch (inp) {
                case "b": {
                    Game game = new Game(new Bot("Bot 1"), new Bot("Bot 2"));
                    game.begin();
                    while (!game.isEnd()) {
                        System.out.println(game.outForBots());
                        game.turn();
                        Game.save("GAMESAVE", game);
                        Thread.sleep(3000);
                    }
                    System.out.println("Winner: " + game.getWinner());
                    break;
                }
                case "h": {
                    Game game = new Game(new Human("Human"), new Bot("Bot"));
                    game.begin();
                    while (!game.isEnd()) {
                        try {
                            System.out.print(game.outForBotAndHuman());
                            System.out.print(game.getPreviousResult() + " >");
                            game.turn();
                            Game.save("GAMESAVE", game);
                        } catch (NumberFormatException e) {
                            System.out.println("Error! Please, input right coordinates [a1-j10]");
                        }
                    }
                    System.out.println("Winner: " + game.getWinner());
                    break;
                }
                default: {
                    System.out.println("Unknown option: " + inp);
                }
            }
        } else {
            Game game = Game.load("GAMESAVE");
            while (!game.isEnd()) {
                game.turn();
                Game.save("GAMESAVE", game);
                System.out.println(game.outForBots());
            }
            System.out.println("Winner: " + game.getWinner());
        }

    }

}
