package game;

import board.Point;

import java.io.Serializable;

public class Turn implements Serializable {

    public final Point point;
    public final TurnResult result;

    public Turn(Point point, TurnResult result) {
        this.point = point;
        this.result = result;
    }
}
