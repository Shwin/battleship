package game;

import board.EnemyBoard;
import board.IBoard;
import players.Bot;
import players.Player;

import java.io.*;

/**
 * Implement main game's logic
 */
public class Game implements Serializable {

    private Player p1, p2, currentPlayer;


    public Game(Player p1, Player p2) {
        this.p1 = p1;
        this.p2 = p2;
        this.currentPlayer = p1;
    }

    public static void save(String path, Game game) {
        File f = new File(path);
        try (ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(f)))) {
            out.writeObject(game);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Game load(String path) {
        File f = new File(path);
        try (ObjectInputStream inp = new ObjectInputStream(new BufferedInputStream(new FileInputStream(f)))) {
            Game game = (Game) inp.readObject();
            inp.close();
            return game;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new Game(new Bot("Bot 1"), new Bot("Bot 2"));
    }

    /**
     * Initialize boards. Must be execute after  constructor.
     */
    public void begin() {
        IBoard board1 = p1.begin();
        IBoard board2 = p2.begin();
        p1.setEnemyBoard(new EnemyBoard(board2));
        p2.setEnemyBoard(new EnemyBoard(board1));
    }

    /**
     * turn or some turns of one player
     */
    public void turn() {
        TurnResult result;
        do {
            result = currentPlayer.turn();
        } while (result != TurnResult.PASS);
        currentPlayer = switchOfPlayer(p1, p2);
    }

    /**
     * next player
     *
     * @param p1
     * @param p2
     * @return
     */
    private Player switchOfPlayer(Player p1, Player p2) {
        return (p1 == currentPlayer) ? p2 : p1;
    }

    /**
     * Has the game ended?
     *
     * @return
     */
    public boolean isEnd() {
        return !(p1.isAlive() && p2.isAlive());
    }

    /**
     * @return name of winner
     */
    public String getWinner() {
        Player winner = (p1.isAlive()) ? p1 : p2;
        return winner.getName();
    }

    /**
     * display situation
     * if play 2 bots
     *
     * @return
     */
    public String outForBots() {
        return "------------------------------------------------\n" +
                this.p1.getMyBoard() + "\n" +
                this.p2.getMyBoard() + "\n" +
                "-------------------------------------------------\n\n";
    }

    /**
     * display situation
     * if play bot and human
     *
     * @return
     */
    public String outForBotAndHuman() {
        return "------------------------------------------------\n" +
                this.p1.getMyBoard() + "\n" +
                this.p1.getEnemyBoard() + "\n" +
                "-------------------------------------------------\n\n";
    }

    /**
     * get previous turn's result for currentPlayer
     *
     * @return
     */
    public TurnResult getPreviousResult() {
        if (currentPlayer.getPreviousTurns().size() == 0) {
            return TurnResult.PASS;
        } else {
            return currentPlayer.getPreviousTurns().peekFirst().result;
        }
    }

}
