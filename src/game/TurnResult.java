package game;

import java.io.Serializable;

/**
 * Result of turn
 */
public enum TurnResult implements Serializable {

    PASS, INJURE, KILL,
}
