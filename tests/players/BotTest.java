package players;

import board.Board;
import board.EnemyBoard;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

public class BotTest {

    Board b;
    EnemyBoard be;
    Bot bot;

    @Before
    public void setUp() throws Exception {
        b = new Board(Bot.genFleetPosition());
        be = new EnemyBoard(b);
        bot = new Bot("One");
        bot.setMyBoard(b);
        bot.setEnemyBoard(be);

    }

    @Test
    public void testChoseArea() throws Exception {
        for (int i = 0; i < 22; i++) {
            bot.turn();
        }
        Assert.assertTrue(be.getBombedAreas().size()<bot.getUnbombedAreas().size());
    }

    @Test
    public void testRemoveEmptyAreas() throws Exception {
        be.fire(2,0);
        bot.removeEmptyAreas(2, 0, new HashSet<>());
        be.fire(4, 0);
        bot.removeEmptyAreas(4,0,new HashSet<>());
        Assert.assertEquals(90,bot.getUnbombedAreas().size());
    }

    @Test
    public void testIsDeadShip() throws Exception {
        be.fire(2,0);
        Assert.assertTrue(be.isDeadShip(2,0));
    }
}