package board;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import players.Bot;

public class BoardTest {

    Board b;
    EnemyBoard be;
    Bot bot;

    @Before
    public void setUp() throws Exception {
        b = new Board(Bot.genFleetPosition());
        bot = new Bot("One");
        bot.setMyBoard(b);
        bot.setEnemyBoard(new EnemyBoard(b));
        be = new EnemyBoard(b);
    }


    @Test
    public void testIsDeadShip() throws Exception {
        Assert.assertFalse(b.isDeadShip(0,0));
    }
}