package board;

import game.TurnResult;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import players.Bot;

public class EnemyBoardTest {

    Board b;
    EnemyBoard be;
    Bot bot;

    @Before
    public void setUp() throws Exception {
        b = new Board(Bot.genFleetPosition());
        bot = new Bot("One");
        bot.setMyBoard(b);
        bot.setEnemyBoard(new EnemyBoard(b));
        be = new EnemyBoard(b);
    }

    @Test
    public void testFire() throws Exception {
        TurnResult result = be.fire(2, 0);
        Assert.assertEquals(TurnResult.KILL, result);
        Assert.assertEquals(3,be.getAliveFleetStatistics()[0]);
        Assert.assertTrue(be.getBombedAreas().contains(new Point(2, 0)));
    }


    /**
     * if (x,y) are cordinats of real ship
     * @throws Exception
     */
    @Test
    public void testIsDeadShipIfShip() throws Exception {
        Assert.assertEquals(TurnResult.KILL, be.fire(2, 0));
        Assert.assertEquals(true,be.isDeadShip(2,0));
    }



    @Test
    public void testIsDeadShipIfFreeSpace() throws Exception {
        Assert.assertFalse(be.isDeadShip(0, 0));
    }

    @Test
    public void testIsDeadShipIfFreeSpaceAfterFire() throws Exception {
        be.fire(0,0);
        be.isDeadShip(0,0);
        Assert.assertFalse(be.isDeadShip(0,0));
    }
}